# Ansible Role: Oh My Zsh

Prerequisite
============

You need to have :

* Docker installed [get docker](https://docs.docker.com/get-docker/)
* Python3 and pip installed
    * On mac go [here](https://python-guide-pt-br.readthedocs.io/fr/latest/starting/install/osx.html)
    * On ubuntu go [here](https://linuxize.com/post/how-to-install-pip-on-ubuntu-18.04/)
* Once installed all above you can install all python dependancies into the [requirements.txt](requirements.txt)

▶️ To install python dependancies please run : 

```bash
pip install -r requirements.txt
```

STEPS:
======

Step 1 🗒: You have to create a docker container, to run ansible task on it.
--------------------------------------------------------------------------

📝 Please copy/paste this snippets into [create_an_ubuntu_container.yml](create_an_ubuntu_container.yml)

```yaml
- hosts: localhost
  tasks:
    - docker_container:
        container_default_behavior: compatibility # which will ensure that the default values are used when the values are not explicitly specified 
        name: my-favourite-container # Your Docker name
        image:  camelotte/python-ubuntu:18.04 # Custom Python Ubuntu image
        command: [
            "sleep", "1d"
        ] # Command to run at the container start
```

We use the ansible plugin : `docker_container` to create a container.
The command `sleep 1d` permit to keep alive the container during 1 day, without that the container will be exited and we
couldn't run task on it.

▶️ To run the ansible task you have to run the command below:

```bash
ansible-playbook create_an_ubuntu_container.yml 
```

This command will run the playbook [create_an_ubuntu_container.yml](create_an_ubuntu_container.yml)

▶️ To test if everything is ok, run :

```bash
docker ps  # Possible need sudo if you have not configured your docker
```

output ✅:

```
CONTAINER ID        IMAGE                           COMMAND             CREATED             STATUS              PORTS               NAMES
e802cf83d4c0        camelotte/python-ubuntu:18.04   "sleep 1d"          15 hours ago        Up 15 hours                             my-favourite-container
```

Step 2 🗒: Configure your unbuntu container before running task on it:
---------------------------------------------------------------------

* First you have to put information about the container into [inventory file](inventory.yml)
    * [Inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) give information to ansible : 
    `On which machines do you want to execute ansible tasks ?`

📝 Copy paste into the inventory
    
```yaml
all:
  hosts:
    my-favourite-container: # Your Docker name
      ansible_connection: docker  # Specify the connection type, here is docker
      ansible_python_interpreter: python
```

* Secondly you have to update the cache of your container and create an user to setup a beautiful Oh-my-zsh shell for 
this user.

📝 Put this snippets into [main](main.yml).

```yaml
- name: Config an ubuntu zsh shell with oh-my-zsh
  hosts: my-favourite-container  # Your Docker name

  vars:
    user_name: toto

  tasks:
    - name: update apt cache
      apt:
        update_cache: yes # It's similar to "apt update"
      changed_when: no

    - name: Add the user {{ user_name }}
      user:
        name: "{{ user_name }}"
        home: "/home/{{ user_name }}"
        createhome: yes
        state: present
```

:warning: *Don't forget to replace your user_name.*

▶️ To run this task you have to run the below command:

```bash
ansible-playbook main.yml -i inventory.yml
```

This command permit to run the playbook [main](main.yml) with the inventory 

Step 3 🗒: Run my_beautiful_zsh role on your container
----------------------------------------------------

* To run the role, you have to call it into your playbook [main](main.yml) like that :

📝 Put it after the written tasks : 

```yaml
  roles:
    - role: my_beautiful_zsh    
```

:warning: Be careful with the indentation ! Role have to be at the same level of tasks !

▶️ To apply the role please run :

```bash
ansible-playbook main.yml -i inventory.yml
```

Test if everything is ok with :

```bash
docker exec -it --user YOUR_USERNAME my-favourite-container zsh
```

And now, you have a shell with oh_my_zsh with some plugins. 

![final results](utils/final_results.png)

💡 If you run again the playbook [main.yml](main.yml) we can see that nothing is in state `changed` :

![ansible recap](utils/ansible_recap.png)

Ansible can detect when there is a changement or not. This makes it possible to avoid to re run some long tasks !

Feel free to add plugins and some customizations 🤓

Annexes:
-------

* Plugins lists : [plugins oh ohmyzsh](https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins)
* ohmyzsh Themes : [themes](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes)
* Next level : [theme from Powerlel10k](https://github.com/romkatv/powerlevel10k)

